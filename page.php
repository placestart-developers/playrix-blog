<?php

get_header();

if (is_page(7))
	get_template_part('./components/page-about');

elseif (is_page(9))
	get_template_part('./components/page-services');

elseif (is_subpage(9))
	get_template_part('./components/page-service');

elseif (is_page(11))
	get_template_part('./components/page-contacts');

else
	get_template_part('./components/page-text');


get_footer();