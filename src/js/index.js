// Assets
import './components/compress-images';

// Libs
import './libs/lightbox.min';
import './libs/mask.min';
import './libs/svg.min';

import './components/preloader';
import './components/adaptation';
import './components/ajax-core';
import './components/menu';
import './components/articles';
