const nowWindowWidth = $(window).width();
const nowWindowHeight = $(window).height();
const speed = 400;

/**
 * Метод раскрытия меню
 *
 * @param {jQuery object} element - кнопка раскытия меню
 */
const menuButtonClick = (element) => {
	// Блокируем нажатие пока не завершится анимация
	if (element.hasClass('blocked')) return false;

	if (element.hasClass('menu__button--close')) {
		$('.menu nav').animate({
			opacity: '0',
			left: '30px'
		}, speed, () => {
			$('.menu').removeClass('visibility');
			$('.menu__background').animate({
				opacity: '0',
				width: '0',
				height: '0'
			}, speed, () => {
				$('.menu__background').remove();
				element.removeClass('blocked');

				element.removeClass('menu__button--close');
				document.body.style.overflow = '';
			});
		});
	} else {
		document.body.style.overflow = 'hidden';
		element.addClass('menu__button--close');

		$('body').append('<div class="menu__background"></div>');
		$('.menu__background').animate({
			opacity: '1',
			width: '100%',
			height: '140%'
		}, speed, () => {
			$('.menu').addClass('visibility');
			$('.menu nav').animate({
				opacity: '1',
				left: '0'
			}, speed);
			element.removeClass('blocked');
		});
	}

	// Блокировка кнопки раскрытия, закрытия меню
	element.addClass('blocked');
}

$('body').on('click', '.menu__button', function() {
	menuButtonClick($(this));
});

$('.menu .menu-item-has-children').hover(function () {
	if (nowWindowWidth >= 1200)
		$(this).children('.sub-menu').stop(true, false).fadeIn(speed / 2).css('display', 'flex');
}, function () {
	if (nowWindowWidth >= 1200)
		$(this).children('.sub-menu').stop(true, false).fadeOut(speed / 2);
});

$('body').on('click', '.menu .menu-item-has-children > a', function (event) {
	event.preventDefault();
	if (nowWindowWidth < 1200) {
		$(this).closest('nav').css('overflow', 'hidden');
		$(this).siblings('.sub-menu').fadeIn(speed / 2);
	}
});

$('body').on('click', '.menu ul a', function () {
	if (nowWindowWidth < 1200 && $(this).siblings('.sub-menu').length) return false;
	menuButtonClick($('.menu__button'));
})

$('.menu ul a').each(function (i, el) {
	$(el).attr('data-activity', 'activity-link');
	if ($(el).closest('li').hasClass('menu-item-has-children')) {
		$(el).siblings('.sub-menu').prepend('<button class="sub-menu__close" type="button">Назад</button>');
	}
});

$('body').on('click', '.sub-menu__close', function () {
	$(this).closest('.sub-menu').fadeOut(speed / 2);
	$(this).closest('nav').css('overflow', '');
})
