const documentBorderTop = 0
const documentBorderBottom = $(document).height()

const $post = $('.post')
const $content = $('.content')
const $subscribe = $('.subscribe')
const $shares = $('.shariff')

if ($subscribe.length) {

    $('body').on('click', '.subscribe-close', (event) => {
        event.preventDefault()

        $subscribe.animate({
            opacity: 0
        }, 200, () => {
            $subscribe.remove()
        })
    })

}

if ($('.post').length) {
    
    $(window).on('load resize', () => {

        if (!$content.length && $subscribe.length)
            $subscribe.remove()

        if ($subscribe.length) {
            positionSubscribe($post, $subscribe, 'bottom')
            // positionShares($post, $shares)

            $(window).scroll(() => {
                positionSubscribe($post, $subscribe, 'bottom')
                // positionShares($post, $shares)
            })
        }
    })

}

const positionSubscribe = ($container, $element, verticalPosition = 'center') => {
    const windowBorderTop = $(window).scrollTop()
    const windowBorderBottom = $(window).scrollTop() + $(window).height()

    let offsetLeft = $content.offset().left + $content.width() + $content.offset().left / 2 - $element.width() / 2 - parseInt($element.css('padding-left'))
    
    $element.css('left', `${ offsetLeft }px`)

    let verticalPositionBorderTop
    switch (verticalPosition) {
        case 'center':
            verticalPositionBorderTop = windowBorderTop - $container.offset().top + $(window).height() / 2 - $element.height() / 2
            break
        case 'bottom':
            verticalPositionBorderTop = windowBorderBottom - $container.offset().top - $element.height() - 70 - 50
            break
    }

    let verticalPositionBorderBottom = verticalPositionBorderTop + $element.height()

    if (verticalPositionBorderTop >= 70 && verticalPositionBorderBottom <= $container.height() - 70) {
        $element.css('top', verticalPositionBorderTop)
    }

    if (verticalPositionBorderBottom >= $container.height() - 70) {
        $element.css('top', $container.height() - $element.height() - 70)
    }
}

const positionShares = ($container, $element) => {
    const windowBorderTop = $(window).scrollTop()
    const windowBorderBottom = $(window).scrollTop() + $(window).height()
    
    $element.css({
        'position': 'absolute',
        'right': '15px',
        'margin': '0'
    })

    let verticalPositionBorderTop = windowBorderTop - $container.offset().top + $(window).height() / 2 - $element.height() / 2 - 30
    let verticalPositionBorderBottom = verticalPositionBorderTop + $element.height()

    if (verticalPositionBorderTop >= 70 && verticalPositionBorderBottom <= $container.height() - 70) {
        $element.css('top', verticalPositionBorderTop)
    }

    if (verticalPositionBorderBottom >= $container.height() - 70) {
        $element.css('top', $container.height() - $element.height() - 70)
    }
}

$(window).on('load resize', () => { resizeTextElements() })

const resizeTextElements = () => {
    if (!$('.content-text').length) return false

    $('.gallery .item-thumbnail').each((i, e) => {
        var w_gallery_img = $(e).width()
        var h_gallery_img = w_gallery_img / 1.5
        $(e).css('height', h_gallery_img)
    })
    $('.certificates .certificate-thumbnail').each((i, e) => {
        var w_gallery_img = $(e).width()
        var h_gallery_img = w_gallery_img * 1.5
        $(e).css('height', h_gallery_img)
    })
}