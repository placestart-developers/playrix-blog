import 'dotdotdot';
import jQueryBridget from 'jquery-bridget/jquery-bridget';
import Masonry from 'masonry-layout';

let $container;

jQueryBridget('masonry', Masonry, $);

$(window).on('load', () => {
    $('.articles__last-artcle__content, .article__info').dotdotdot();
    artcilesInit();
})

function artcilesInit ()
{
    $container = $('.articles > .container').masonry({
        itemSelector: '.masonry-item',
        columnWidth: '.masonry-item__column',
        percentPosition: true
    }).addClass('masonry-loaded');
}

export const addArticles = (response) => {
    var $response = $(response);
    $container
        .append($response)
        .masonry('appended', $response)
        .masonry();
}
