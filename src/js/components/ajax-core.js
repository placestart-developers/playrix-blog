import { getUrlVar as getUrlParams } from './get-url-params'
import { addArticles } from './articles';

let urlParams = getUrlParams()

let $articles = $('.articles')
let $ajaxLoader = $('.ajax-loader')
let offset = urlParams['offset'] ? urlParams['offset'] : 0

if ($('body').hasClass('home') || $('body').hasClass('archive')) {

    $(window).on('scroll', (event) => {
        if ($articles.hasClass('articles-ended')) return false

        const positionWindowBorderTop = $(window).scrollTop()
        const positionWindowBorderBottom = $(window).scrollTop() + $(window).height()

        if (positionWindowBorderBottom >= $articles.offset().top + $articles.height()) {
            if ($articles.hasClass('ajax-blocked')) return false
            $articles.addClass('ajax-blocked')
            offset++

            var link = window.location.href
            if (link.indexOf('?') != -1) {
                link = link.substr(0,link.indexOf('?'))
            }
            link += '?offset=' + offset

            $ajaxLoader.addClass('loading')
            $ajaxLoader.fadeIn(200).css('display', 'flex')

            bottomless(link)
        }
    })

}

const bottomless = (link) => {
    let data = {
        action: 'bottomless',
        referer: link
    }

    $.post( myajax.url, data, function(response) {
        history.pushState('', '', link)

        if (!response) {
            $articles.addClass('articles-ended')
            $ajaxLoader.remove();
        }

        addArticles($(response));

        $ajaxLoader.fadeOut(500)

        setTimeout(() => {
            $articles.removeClass('ajax-blocked')
        }, 800)
    })
}