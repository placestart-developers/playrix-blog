<?php if ( ! defined('FIX_AJAX_DOING') ) : ?>

<?php get_header(); ?>

<section class="articles">
    <div class="container">
        <div class="masonry-item__column"></div>
        <?php endif; ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php get_template_part('parts/articles-body'); ?>
        <?php endwhile; ?>
        <?php if ( ! defined('FIX_AJAX_DOING') ) : ?>

        <div class="ajax-loader">
            <svg role="img"><use xlink:href="<?php bloginfo( 'template_url' ); ?>/assets/images/sprite.svg#ajax"></use></svg>
        </div>
    </div>
</section>

<?php get_footer(); ?>

<?php endif; ?>