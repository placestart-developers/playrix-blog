<?php if ( ! defined('FIX_AJAX_DOING') ) : ?>

<?php get_header(); ?>

<section class="articles">
    <div class="container">
        <div class="masonry-item__column"></div>

        <?php
            if (get_field('first-post', 'option') == 'select')
                $query = new WP_Query(array( 'p' => get_field('first-post-select', 'option') ));
            else
                $query = new WP_Query(array( 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1 ));
        ?>
        <?php while ($query->have_posts()) : $query->the_post(); ?>

            <?php $cat = get_the_category(); ?>
            <div class="masonry-item masonry-item__width-2 masonry-item__first">
                <div class="articles__last-artcle" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
                    <div class="articles__last-artcle__body">
                        <a class="articles__last-artcle__content" href="<?php the_permalink(); ?>">
                            <p class="title"><?php the_title(); ?></p>
                            <p class="date"><?php echo date("d F, Y", strtotime($post->post_date)); ?></p>
                            <p class="desc"><?php the_field('description'); ?></p>
                        </a>
                        <div class="articles__last-artcle__post-info">
                            <a class="cat" href="<?php echo get_category_link($cat[0]->term_id); ?>"><?php echo $cat[0]->name; ?></a>
                            <?php echo do_shortcode('[shariff backend="on"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; wp_reset_query(); ?>

        <div class="masonry-item">
            <?php get_template_part('parts/subscribe'); ?>
        </div>

        <div class="masonry-item">
            <div class="company-info">
                <p class="title">Стань частью нашей команды</p>
                <div class="company-info__indicators">
                    <?php while (have_rows('team-repiter', 'option')) : the_row(); ?>
                        <div class="indicator">
                            <p class="number"><?php the_sub_field('team-value'); ?></p>
                            <p class="sign"><?php the_sub_field('team-subtitle'); ?></p>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php $link = get_field('vacancies-link', 'option'); ?>
                <a class="the_button" href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ? $link['title'] : 'Перейти к вакансиям'; ?></a>
            </div>
        </div>

        <?php endif;
        get_template_part('parts/index-articles');
        if ( ! defined('FIX_AJAX_DOING') ) : ?>
        <div class="ajax-loader">
            <svg role="img"><use xlink:href="<?php bloginfo( 'template_url' ); ?>/assets/images/sprite.svg#ajax"></use></svg>
        </div>
    </div>
</section>

<?php get_footer(); ?>

<?php endif; ?>