<?php

/**
 * Запрещаем вызов файла по прямому url
 */
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Shares fixed
 */
define( 'SHARIFF_FRONTENDS', $_SERVER['SERVER_NAME'] );

require_once('includes/ajax-core.php');
require_once('includes/Content.php');


if (function_exists('register_sidebar')) register_sidebar();
if (function_exists('add_theme_support')) add_theme_support('menus');
if (function_exists('add_theme_support')) add_theme_support('post-thumbnails');


// Remove Canonical Link Added By Yoast WordPress SEO Plugin
function at_remove_dup_canonical_link() {
    return false;
}


add_filter( 'wpseo_canonical', 'at_remove_dup_canonical_link' );

remove_filter( 'the_content', 'wpautop' );// для контента


function get_file_info( $file_info ) {

    $mime_types = array(
        'application/msword'            => 'doc',
        'image/jpeg'                    => 'jpg',
        'application/pdf'               => 'pdf',
        'image/png'                     => 'png',
        'application/vnd.ms-powerpoint' => 'ppt',
        'application/x-rar-compressed'  => 'rar',
        'image/tiff'                    => 'tiff',
        'text/plain'                    => 'txt',
        'application/vnd.ms-excel'      => 'xls',
        'application/zip'               => 'zip',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'   => 'docx',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'         => 'xlsx',
    );

    $file_size                          = array( 'b', 'kb', 'Mb' );
    $file_info_output                   = array();
    $file_info_output[ 'size' ]    = filesize( get_attached_file( $file_info['id'] ) );

    $i = 0;

    while( $file_info_output[ 'size' ] > 1024 ) {
        $file_info_output[ 'size' ] = $file_info_output[ 'size' ] / 1024;
        $i++;
    }

    $file_info_output[ 'url' ]  = $file_info[ 'url' ];
    $file_info_output[ 'size' ] = round($file_info_output[ 'size' ], 2) . " " . $file_size[$i]; // Размер файла
    $file_info_output[ 'mime' ] = $mime_types[ $file_info[ 'mime_type' ] ]; // Расширение файла

    return $file_info_output;
}


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Сайт',
        'menu_title'    => 'Сайт',
        'menu_slug'     => 'site',
        'capability'    => 'edit_posts',
        'icon_url'      => 'dashicons-welcome-write-blog',
        'redirect'      => false,
    ));


}
add_filter( 'wpcf7_validate_configuration', '__return_false' );


if( ! function_exists( 'is_subpage' ) ) {
    /**
     * Функция проверяет текущий объект, является ли он подстраницей
     * @param integer $post_parent_id ИД родительской страницы, если необходимо
     *     boolean Или false, если данный атрибут необходимо пропустить
     * @param WP_Post $post Объект записи, если необходимо
     * null Или null, если необходимо опустить параметр
     * @return boolean Возвращает результат проверки
     * integer Или ИД родителя, если $post_parent_id = false
     */
    function is_subpage( $post_parent_id = false, $post = null ) {
        if( is_null( $post ) ) global $post;
        if( ! is_page() ) return false;

        if( $post->post_parent ) {
            if( $post_parent_id ) {
                if( $post->post_parent != $post_parent_id && $post->post_parent > 0 ) {
                    return is_subpage( $post_parent_id, get_post( $post->post_parent ) );
                } else return true;
            } else return $post->post_parent;
        } else {
            return false;
        }
    }
}
