const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BabiliPlugin = require("babili-webpack-plugin");
const tinyPngWebpackPlugin = require('tinypng-webpack-plugin');

// Стандартные пути
var paths = {
    src: {
        self:      './src',
        js:        './src/js',
        sass:      './src/sass',
        images:    './src/images'
    },
    assets: {
        self:      './assets',
        js:        './assets/js',
        css:       './assets/css',
        images:    './assets/images'
    },
}

module.exports = [
    {
        entry: paths.src.js + '/index',
        output: {
            path: path.resolve(__dirname, paths.assets.js),
            filename: 'bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "babel-loader",
                    options: {
                        presets: ['es2017']
                    }
                },

                // Минификация картинок
                {
                    test: /\.(gif|png|jpe?g)$/i,
                    exclude: /(node_modules|bower_components)/,
                    loaders: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '../images/[name].[ext]?[hash]',
                                publicPath: paths.assets.images
                            }
                        },
                        'image-webpack-loader'
                    ]
                }
            ]
        },
        plugins: [
            // Минификация откомпилированного файла
            new BabiliPlugin(),

            // Глобальное подключение jquery
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Util: "exports-loader?Util!bootstrap/js/dist/util",
                Tab: "exports-loader?Tab!bootstrap/js/dist/tab"
            }),

            // Минификация картинок
            new tinyPngWebpackPlugin({
                key: [
                    'vdNQsRcaq1jqKKexKikVRpH2XZCR899o',
                    'H5jueMufbG7zvACNHrMVO_Bsdx7asbkO',
                    '-GTyp8jVYveO3A7EvOcjoHKfVT96QmVw',
                    '_LM_GoX2k65ea8cyHjLr9CvMW8kHmW1O'
                ],
                relativePath: paths.assets.images,
                ext: ['png', 'jpeg', 'jpg']
            })
        ]
    }
];
