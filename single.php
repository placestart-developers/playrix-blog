<?php get_header(); ?>

<?php $cat = get_the_category(); ?>

<section class="post">
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-12">
                <?php echo do_shortcode('[shariff backend="on"]'); ?>
            </div>
            <div class="col-lg-8 col-12">
                <div class="post__info">
                    <a class="category" href="<?php echo get_category_link($cat[0]->term_id); ?>"><?php echo $cat[0]->name; ?></a>
                    <h1 class="title"><?php the_title(); ?></h1>
                    <div class="group">
                        <p class="author"><?php the_author(); ?></p>
                        <p class="date"><?php echo date("d F, Y", strtotime($post->post_date)); ?></p>
                    </div>
                </div>
                <?php the_custom_content(); ?>

                <div class="post__similars">
                    <p class="post__similars-title">Статьи по теме</p>
                    <div class="row">
                        <?php $query = new WP_Query(array( 'post_type', 'category_name' => $cat[0]->slug, 'orderby' => 'rand', 'post__not_in' => array( $post->ID ) )); ?>
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <div class="col-md-6 col-12">
                                <a class="similar" href="<?php the_permalink(); ?>">
                                    <p class="category"><?php echo $cat[0]->name; ?></p>
                                    <p class="title"><?php the_title(); ?></p>
                                </a>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php get_template_part('parts/subscribe'); ?>

</section>

<?php get_footer(); ?>