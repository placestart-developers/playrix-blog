
<div class="sidebar">
    <?php
        $ancestors = get_ancestors($post->ID, 'page');
        $ID = get_the_ID();
        if ($ancestors[count($ancestors)-1])
        {
            echo '<ul>';
                $children = new WP_Query(array( 'post_type' => 'page', 'post_parent' => $ancestors[count($ancestors)-1], 'orderby' => 'menu_order', 'order' => 'ASC' ) );
                while($children->have_posts())
                {
                    $children->the_post();
                    $class = $ID == get_the_ID() ? 'current-menu-item' : '';
                    echo '<li class="'. $class .'"><a href="'. get_the_permalink() .'">'. get_the_title() .'</a></li>';
                }
            echo '</ul>';
        }
        elseif ( get_field('rtr') )
        {
            
        }
        else
            wp_nav_menu( array('menu' => 'sidebar', 'container' => '', 'items_wrap' => '<ul id="%1$s" >%3$s</ul>',) );
    ?>
</div>
