<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/assets/images/favicon.ico" type="image/x-icon">

        <title><?php wp_title(); ?> <?php bloginfo('name'); ?></title>

        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

        <?php wp_head(); ?>

        <style>
            html { margin: 0 !important; }
        </style>
    </head>
    <?php
        if (is_home()) $class = 'home';
        else $class = '';
    ?>
    <body <?php body_class($class); ?>>

        <?php get_template_part('parts/menu'); ?>

        <div class="wrapper">

            <main>
                <header class="header">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-9">
                                <a class="header__logo" href="/">
                                    <svg role="img"><use xlink:href="<?php bloginfo( 'template_url' ); ?>/assets/images/sprite.svg#logo-blog"></use></svg>
                                </a>
                            </div>
                            <div class="col d-none d-lg-block">
                                <?php wp_nav_menu(array('menu' => 'main-menu', 'container' => '', 'items_wrap' => '<ul id="%1$s" class="header__menu">%3$s</ul>',)); ?>
                            </div>
                            <div class="col-3 d-lg-none">
                                <div class="menu__button"><div></div></div>
                            </div>
                        </div>
                    </div>
                </header>
