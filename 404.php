<?php get_header(); ?>

<section class="not-found">
	<div class="container">
		<div class="row">
			<div class="col-xl-8 col-sm-10 col-12 m-auto">
				<p class="the_title the_title--big the_title--white">Страница не найдена</p>
				<p class="desc">Выберите интересующий раздел в меню <br>или перейдите на главную страницу сайта</p>
				<a class="the_button" href="/">Перейти на главную</a>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>