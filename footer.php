            </main>
            <footer class="footer">
                <div class="container">
                    <div class="footer__head">
                        <div class="row">
                            <div class="col-lg-2 col-12">
                                <a class="footer__logo" href="https://www.playrix.ru/">
                                    <svg role="img"><use xlink:href="<?php bloginfo( 'template_url' ); ?>/assets/images/sprite.svg#logo"></use></svg>
                                </a>
                            </div>
                            <div class="col-lg-7 d-none d-lg-block">
                                <?php wp_nav_menu(array('menu' => 'footer-menu', 'container' => '', 'items_wrap' => '<ul id="%1$s" class="footer__menu">%3$s</ul>',)); ?>
                            </div>
                            <div class="col-lg-3 col-12">
                                <div class="footer__social-networks">
                                    <?php
                                    $fb      = get_field('footer-facebook', 'option');
                                    $vk      = get_field('footer-vk', 'option');
                                    $yt      = get_field('footer-youtube', 'option');
                                    $in      = get_field('footer-linkedin', 'option');
                                    $inst    = get_field('footer-instagram', 'option');
                                    $tw      = get_field('footer-twitter', 'option');
                                    ?>
                                    <a href="<?php echo $fb['url']; ?>" target="<?php echo $fb['target'] ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/fb.png" alt=""></a>
                                    <a href="<?php echo $vk['url']; ?>" target="<?php echo $vk['target'] ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/vk.png" alt=""></a>
                                    <a href="<?php echo $yt['url']; ?>" target="<?php echo $yt['target'] ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/yt.png" alt=""></a>
                                    <a href="<?php echo $in['url']; ?>" target="<?php echo $vk['target'] ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/in.png" alt=""></a>
                                    <a href="<?php echo $inst['url']; ?>" target="<?php echo $inst['target'] ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/inst.png" alt=""></a>
                                    <a href="<?php echo $tw['url']; ?>" target="<?php echo $tw['target'] ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/tw.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <?php if (is_home()) : ?>
                            <img class="dragon-home" src="<?php bloginfo('template_url'); ?>/assets/images/dragon-1.svg" alt="">
                        <?php else : ?>
                            <img class="dragon" src="<?php bloginfo('template_url'); ?>/assets/images/dragon-2.svg" alt="">
                        <?php endif; ?>
                    </div>
                    <div class="footer__copyright">
                        <p>©<?php $date = (int) date('Y'); $date_start = 2017; print "&nbsp;" . $date_start; if( $date > $date_start ) print " - ".$date; ?>. Playrix. Все права защищены</p>
                    </div>
                </div>
            </footer>
        </div>

        <script src="<?php bloginfo('template_url'); ?>/assets/js/bundle.js"></script>

        <?php do_action('wp_footer'); ?>
    </body>
</html>
