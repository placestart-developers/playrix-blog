<?php

add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data ()
{
	wp_localize_script('jquery', 'myajax',
		array(
			'url' => admin_url('admin-ajax.php'),
		)
	);
}

/**
 * Обработчик Ajax-запроса по достижение конца сайта
 */
function bottomless ()
{
	$str = file_get_contents($_POST['referer']."&bottomless=1");
	$str = str_replace('&', '', $str);
	$str = str_replace('#038;', '', $str);
	print preg_replace('/(bottomless\=1)/', '', $str);
	wp_die();
}
add_action('wp_ajax_bottomless', 'bottomless');
add_action('wp_ajax_nopriv_bottomless', 'bottomless');


if (isset($_GET['bottomless'])) {
	unset($_GET['bottomless']);
    define('FIX_AJAX_DOING', true);
}

function fixArchiveAjaxPaginate(&$query) {
    $isAjax     = defined('FIX_AJAX_DOING');
    $haveOffset = isset($_GET['offset']);
    $isMain     = $query->is_main_query();
    $isCat      = $query->is_category();
    if ($isAjax && $haveOffset && $isMain && $isCat) {
        $query->query_vars['paged'] = (int) $_GET['offset'];
    } elseif (!$isAjax && $haveOffset && $isMain && $isCat) {
        $offset = (int) $_GET['offset'] + 1;
        $query->set('posts_per_page', $offset * get_option('posts_per_page'));
    }
}
add_action('pre_get_posts', 'fixArchiveAjaxPaginate');