<?php

function the_custom_content()
{
	echo '<div class="content">';
		while( have_rows('content') )
		{
			the_row();
			
			// Текст
			if (get_row_layout() == 'content-text') {
				echo '<div class="content-text">'.get_sub_field('text').'</div>';
			}

			// Галерея
			elseif (get_row_layout() == 'content-gallery') {
				?>
					<?php $images = get_sub_field('gallery'); ?>
					<?php if($images) : ?>
						<div class="gallery">
							<div class="row">
								<?php $i = 0; foreach( $images as $image ) : $i++; ?>
									<div class=" col-sm-<?php the_sub_field('gallery-count'); ?> col-6">
										<div class="item">
											<a class="item-thumbnail" href="<?php echo $image['url']; ?>" data-lightbox="gallery-set" style="background-image: url('<?php echo $image['url']; ?>');"></a>
											<div class="item-sign">
												<p><?php echo $image['caption']; ?></p>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
				<?php
			}
		}
	echo '</div>';
}