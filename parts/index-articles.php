<?php

$offset = !empty($_GET['offset']) ? $_GET['offset'] : 1;
if (defined('FIX_AJAX_DOING')) {
    $postsPerPage = get_field('show-posts', 'option') ? get_field('show-posts', 'option') : 10;
    $postsOffset = $postsPerPage * $offset;
} else {
    if (get_field('show-posts', 'option'))
        $postsPerPage = get_field('show-posts', 'option') * $offset;
    else
        $postsPerPage = 10 * $offset;
    $postsOffset = 0;
}

if (get_field('first-post', 'option') == 'select')
    $query = new WP_Query(array(
        'post_type' => 'post',
        'post__not_in' => array( get_field('first-post-select', 'option') ),
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => $postsPerPage,
        'offset' => $postsOffset
    ));
else
    $query = new WP_Query(array( 'post_type' => 'post',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => $postsPerPage,
        'offset' => 1 + $postsOffset
    ));

while ($query->have_posts()) { $query->the_post();
    get_template_part('parts/articles-body');
}