<div class="subscribe">
    <?php if (!is_home()) : ?>
        <a class="subscribe-close" href="">
            <svg role="img"><use xlink:href="<?php bloginfo( 'template_url' ); ?>/assets/images/sprite.svg#cross"></use></svg>
        </a>
    <?php endif; ?>
    <p class="title">Подпишись на наш блог</p>
    <p class="desc">и получай новые посты по почте</p>
    <div class="subscribe__form">
        <?php echo do_shortcode('[contact-form-7 id="4" title="Подпишись на наш блог"]'); ?>
    </div>
</div>