<div class="menu">
	<div class="container">
		<nav>
            <?php wp_nav_menu( array('menu' => 'main-menu', 'container' => '', 'items_wrap' => '<ul id="%1$s" >%3$s</ul>',) ); ?>
            <?php wp_nav_menu( array('menu' => 'footer-menu', 'container' => '', 'items_wrap' => '<ul id="%1$s" >%3$s</ul>',) ); ?>
		</nav>
	</div>
</div>