<?php
    if (defined('FIX_AJAX_DOING')) {
        $class = 'hidden';
    } else {
        $class = '';
    }
    $cat = get_the_category();
?>
<div class="masonry-item article-wrapper <?php echo $class; ?>">
    <div class="article <?php if (!get_the_post_thumbnail_url()) echo 'none-thumbnail'; ?>">
        <a class="article__content" href="<?php the_permalink(); ?>">
            <?php if (get_the_post_thumbnail_url()) : ?>
                <div class="thumbnail" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
            <?php endif; ?>
            <div class="article__info">
                <p class="title"><?php the_title(); ?></p>
                <p class="date"><?php echo date("d F, Y", strtotime($post->post_date)); ?></p>
                <p class="desc"><?php the_field('description'); ?></p>
            </div>
        </a>
        <div class="article__post-info">
            <a class="cat" href="<?php echo get_category_link($cat[0]->term_id); ?>"><?php echo $cat[0]->name; ?></a>
            <?php echo do_shortcode('[shariff backend="on"]'); ?>
        </div>
    </div>
</div>