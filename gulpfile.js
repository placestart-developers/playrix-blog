// Общие модули
var path            = require('path');
var gulp            = require('gulp');
var notify          = require("gulp-notify");          // Обработчик ошибок
var sourcemaps      = require('gulp-sourcemaps');
var rename          = require('gulp-rename');          // Переименование файла
var concat          = require('gulp-concat');          // Конкатенация
var watch           = require('gulp-watch');
    
// Работа с SASS
var sass            = require('gulp-sass');            // Компилятор Sass
var autoprefixer    = require('gulp-autoprefixer');

// Работа с JS
var uglify          = require('gulp-uglify');          // Минификация JS файла

// Работа с SVG
var svgstore        = require('gulp-svgstore');        // Объединение SVG в спрайт
var svgmin          = require('gulp-svgmin');          // SVG минификация
var cheerio         = require('gulp-cheerio');         // Работа с атрибутами


// Пути к исходным файлам
var paths = {
    src: {
        self:      './src',
        js:        './src/js',
        sass:      './src/sass',
        svg:    './src/images/svg'
    },
    assets: {
        self:      './assets',
        js:        './assets/js',
        css:       './assets/css',
        images:    './assets/images'
    },
    node: 'node_modules/'
}


/* Sass компиляция
---------------------------------------*/
gulp.task('sass', function () {
    // Компиляция библиотек
    gulp
        .src([
            paths.src.sass    +'/lib/*.+(scss|sass)',
            paths.node         +'slick-carousel/slick/slick.scss',
        ])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(concat('app.css'))
        .pipe(gulp.dest(paths.assets.css));

    // Компиляция всего остального
    gulp
        .src([
                 paths.src.sass    +'/**/*.+(scss|sass)',
            '!'+ paths.src.sass    +'/lib/*.+(scss|sass)'
        ])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        // .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(sourcemaps.write('./', { sourceRoot: '/source' }))
        .pipe(gulp.dest(paths.assets.css))
});


/* Конкатенация и минификация JS
---------------------------------------*/
gulp.task('js', function () {
    gulp
        .src([
            paths.node       +'jquery/dist/jquery.js',
            paths.node       +'popper.js/dist/umd/popper.js',
            paths.node       +'bootstrap/dist/js/bootstrap.js',
            paths.node       +'slick-carousel/slick/slick.js',
            paths.src.js    +'/libs/*.js',
        ])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.src));
});


/* Объединение SVG файлов
---------------------------------------*/
gulp.task('svg', function () {
    gulp
        .src(paths.src.svg    +'/**/*.svg', { base: 'src/sprite' })
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(rename(function (path) {
            var name = path.dirname.split(path.sep);
            name.push(path.basename);
        }))
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(cheerio({
            run: function ($) {
                $('svg').attr('style', 'display:none');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(gulp.dest(paths.assets.images));
});

gulp.task('watch', function () {
    gulp.watch (paths.src.sass      +'/**/*.+(scss|sass)', ['sass']);
    gulp.watch (paths.src.svg    +'/**/*.svg', ['svg']);
});

gulp.task('default', ['sass', 'watch']);
gulp.task('assets', ['js', 'svg']);